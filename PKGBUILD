# Maintainer: Philip Müller <philm@manjaro.org>
# Maintainer: Bernhard Landauder <bernhard@manjaro.org>
# Contributor: Niklas Pulina <pulina@softmaker.de>
# Author of AUR PKGBUILD for FreeOffice: Muflone http://www.muflone.com/contacts/english/

pkgbase=softmaker-office
_ver=2018
pkgname=${pkgbase}-${_ver}
pkgver=982
pkgrel=1
epoch=1
pkgdesc="A complete, reliable, lightning-fast and Microsoft Office-compatible office suite with a word processor, spreadsheet, and presentation graphics software."
arch=('x86_64')
url="https://www.softmaker.com/"
license=('custom')
depends=('libxrandr' 'libgl' 'xdg-utils' 'gtk-update-icon-cache'
         'desktop-file-utils' 'curl')
conflicts=("${pkgbase}")
replaces=("${pkgbase}")
makedepends=('chrpath')
install="${pkgbase}.install"
source=("https://www.softmaker.net/down/${pkgname}-${pkgver}-amd64.tgz"
        "${pkgname}-textmaker"
        "${pkgname}-planmaker"
        "${pkgname}-presentations"
        "${pkgname}-textmaker.desktop"
        "${pkgname}-planmaker.desktop"
        "${pkgname}-presentations.desktop")
sha256sums=('038e6afe2ad6c4a4cbb33c9ded1bf9e967aa9301ed5a0e3363d615c6a03f2a64'
            '4084836d5e7a45bca5f20558e9cc67eac532e8c6731bee6ca614760aec84caf5'
            'ae90878e9ed0517856abab765246e76cb471be0019e624e734cb0d2d404d06b4'
            'f9f476265f65cc89ff469f0c22f7cf10ddbd9619cd98604c25c6bbcf1796adfd'
            '72b2a3a2626aeb9f2f5d99f16a4bd6823d975995980cfdb65be11f70d9df4cd4'
            '87134719f6a911a9508e5a9c712f161586bec56f8bbb705daac26f40667e302e'
            '0497402cb904ca8736ffd81b97f5ec8ad0fc140557ea6e35a7972a3bc485a9f7')

prepare() {
  xz -d "office$_ver.tar.lzma"
}

build() {
  [ -d "${pkgname}" ] && rm -rf "${pkgname}"
  mkdir "${pkgname}"
  tar x -f "office$_ver.tar" -C "${pkgname}"
  # Remove insecure RPATH
  cd "${pkgname}"
  chrpath --delete "textmaker"
  chrpath --delete "planmaker"
  chrpath --delete "presentations"
}

package() {
  # Install package files
  install -d "${pkgdir}/usr/share"
  cp -r "${srcdir}/${pkgname}" "${pkgdir}/usr/share"
  # Installing license file
  install -d "${pkgdir}/usr/share/licenses/${pkgname}"
  ln -s "/usr/share/${pkgname}/mime/copyright" "${pkgdir}/usr/share/licenses/${pkgname}/license.txt"
  # Install affiliate link
  echo "freeoffice-2021-l-manjaro" > "${pkgdir}/usr/share/${pkgname}/affiliate.txt"
  # Install program executables
  install -d "${pkgdir}/usr/bin"
  install -m 755 -t "${pkgdir}/usr/bin" "${srcdir}/${pkgname}-planmaker"
  install -m 755 -t "${pkgdir}/usr/bin" "${srcdir}/${pkgname}-textmaker"
  install -m 755 -t "${pkgdir}/usr/bin" "${srcdir}/${pkgname}-presentations"
  # Installing icons
  for size in 16 32 48
  do
    install -d "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
    ln -s "/usr/share/${pkgname}/icons/pml_${size}.png" "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}-planmaker.png"
    ln -s "/usr/share/${pkgname}/icons/prl_${size}.png" "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}-presentations.png"
    ln -s "/usr/share/${pkgname}/icons/tml_${size}.png" "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}-textmaker.png"

    install -d "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/mimetypes"
    ln -s "/usr/share/${pkgname}/icons/pmd_${size}.png" "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/mimetypes/application-x-sm-${_ver}-pmd.png"
    ln -s "/usr/share/${pkgname}/icons/prd_${size}.png" "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/mimetypes/application-x-sm-${_ver}-prd.png"
    ln -s "/usr/share/${pkgname}/icons/tmd_${size}.png" "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/mimetypes/application-x-sm-${_ver}-tmd.png"
  done
  # Install desktop files
  install -d "${pkgdir}/usr/share/applications"
  install -m 755 -t "${pkgdir}/usr/share/applications" "${pkgname}-textmaker.desktop"
  install -m 755 -t "${pkgdir}/usr/share/applications" "${pkgname}-planmaker.desktop"
  install -m 755 -t "${pkgdir}/usr/share/applications" "${pkgname}-presentations.desktop"
  # Installing mime files
  install -d "${pkgdir}"/usr/{lib,share}/mime/packages
  ln -s "/usr/share/${pkgname}/mime/${pkgname}.xml" "${pkgdir}/usr/share/mime/packages/${pkgname}.xml"
  ln -s "/usr/share/${pkgbase}/mime/${pkgname}.mime" "${pkgdir}/usr/lib/mime/packages/${pkgname}"
  install -d "${pkgdir}/usr/share/lintian/overrides/"
  ln -s "/usr/share/${pkgname}/mime/${pkgname}.overrides" "${pkgdir}/usr/share/lintian/overrides/${pkgname}"
  # Fix mime conflict with FreeOffice
  sed -i -e 's|icon name="application-x-pmd"|icon name="application-x-sm-${_ver}-pmd"|g' "${pkgdir}/usr/share/${pkgname}/mime/${pkgname}.xml"
  sed -i -e 's|icon name="application-x-prd"|icon name="application-x-sm-${_ver}-prd"|g' "${pkgdir}/usr/share/${pkgname}/mime/${pkgname}.xml"
  sed -i -e 's|icon name="application-x-tmd"|icon name="application-x-sm-${_ver}-tmd"|g' "${pkgdir}/usr/share/${pkgname}/mime/${pkgname}.xml"
  # Fix overrides
  sed -i -e "s|/usr/share/office|/usr/share/${pkgname}|g" "${pkgdir}/usr/share/${pkgname}/mime/${pkgname}.overrides"
}
